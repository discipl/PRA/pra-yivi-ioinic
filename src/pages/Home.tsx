import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import "./Home.css";
import { useState } from "react";
import YiviFrontEnd from "@privacybydesign/yivi-frontend";
import "@privacybydesign/yivi-css";

interface YiviWebInstance {
  abort: Function;
  start: Function;
}

interface YiviResult {
  token: string;
  type: string;
  status: string;
  proofStatus: string;
  disclosed: YiviResultDisclosed[][];
}
interface YiviResultDisclosed {
  id: string;
  issuancetime: number;
  rawvalue: string;
  status: string;
  value: YiviResultDisclosureValue;
}
interface YiviResultDisclosureValue {
  [languageKey: string]: string;
}

const identifiersRequestedAttributes = [
  "irma-demo.discipl.demoBRI.registeredIncome",
  "irma-demo.discipl.demoBRI.taxYear",
];
const Home: React.FC = () => {
  const [yiviWebInstance, setYiviWebInstance] = useState<
    YiviWebInstance | undefined
  >(undefined);
  const [resultValues, setResultValues] = useState<string[] | undefined>(
    undefined
  );

  const startYivi = async () => {
    setResultValues(undefined);

    const yiviWeb = YiviFrontEnd.newWeb({
      debugging: true,
      element: "#yivi-web-form",
      language: "en",
      translations: {
        header: 'Continue with <i class="yivi-web-logo">Yivi</i>',
        loading: "Just one second please!",
      },
      // Back-end options
      session: {
        // Point this to your IRMA server:
        url: "http://192.168.68.110:8088",

        // Define your disclosure request:
        start: {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            "@context": "https://irma.app/ld/request/disclosure/v2",
            disclose: [[identifiersRequestedAttributes]],
          }),
        },
      },
    });

    yiviWeb
      .start()
      .then((result: YiviResult) => {
        console.log(result);
        const values = result.disclosed[0].map((valueObject, index) => {
          return `${identifiersRequestedAttributes[index]}: ${valueObject.value.nl}`;
        });
        setResultValues(values);
      })
      .catch((error: string) => {
        if (error === "Aborted") {
          console.log("We closed it ourselves, so no problem 😅");
          return;
        }
        console.error("Couldn't do what you asked 😢", error);
      });
    setYiviWebInstance(yiviWeb);
  };

  return (
    <IonPage id="home-page">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Yivi-PRA demo</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonGrid>
          <IonRow>
            <IonCol className="center-col">
              <section className="yivi-web-form" id="yivi-web-form"></section>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <div className="ion-text-center">
                <IonButton
                  onClick={() => {
                    startYivi();
                  }}
                >
                  Start
                </IonButton>
                <IonButton
                  onClick={() => {
                    if (yiviWebInstance) {
                      setResultValues(undefined);
                      yiviWebInstance.abort();
                    }
                  }}
                >
                  Abort
                </IonButton>
              </div>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <div className="ion-text-center">
                <ul>
                  {resultValues?.map((value) => (
                    <li>{value}</li>
                  ))}
                </ul>
              </div>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Home;
