import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
  appId: "nl.discipl.pra",
  appName: "pra yivi PoC",
  webDir: "build",
  bundledWebRuntime: false,
  server: {
    cleartext: true,
  },
  android: {
    allowMixedContent: true,
  },
};

export default config;
