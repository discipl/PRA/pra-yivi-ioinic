# PRA Yivi Requestor PoC

This repository showcases a Yivi Requestor Proof of Concept (PoC) developed using the [Yivi (IRMA) frontend package](https://irma.app/docs/irma-frontend/) and the platform Ionic React. The PoC demonstrates how the Yivi protocol enables attribute requests from a Yivi Self-Sovereign Identity (SSI) wallet within a cross-platform application. The PoC can be used to start Yivi attribute disclosure with the Yivi mobile app.

#### Yivi

Yivi [jievie] (formerly known as IRMA) is a privacy-friendly open-source solution for online authentication and digital signatures. Users can log in, and sign documents by selectively revealing relevant attributes about themselves through the Yivi app on their mobile phones. They have full control over their data and can easily manage their login credentials. Organizations can also utilize IRMA for customer authentication and consent gathering. The Yivi app acts as a personal and flexible passport or wallet, allowing users to disclose only necessary information in different situations. For more information, read the [official Yivi documentation](https://irma.app/docs/what-is-irma/).

#### Requested Yivi attributes

The PoC can request all types of Yivi attributes in every request session.
The attribute type contained in the request must exist in their scheme. There are two types of schemes:

- irma-demo schemes, contains attributes for demo purpose
- pbdf schemes, contains attributes for production purposes.

[Complete index of Yivi all attributes](https://privacybydesign.foundation/attribute-index/en/)

#### Techniques used in this PoC:

- Ionic React
- Capacitor
- Yivi-frontend package

#### Prerequisites

##### Install the Yivi App on your smartphone

1. The first step is to download and install the Yivi App (Android / iOS).
2. The PoC is only used for demo and testing purposes, so the requestor server in this PoC is not an HTTPS server but an HTTP server. To make sure that the Yivi app connects to this server we have to activate [developer mode](https://irma.app/docs/v0.6.1/irma-app/#developer-mode). To activate developer mode, navigate to the "More" menu and tap the App ID label 8 times.
3. Make sure that the Yivi app has access to the server. The port that is used by the server should not be blocked by the network's firewall. And when using a local server it can be required to connect the smartphone to the same local network as the server.

##### Add attributes to the Yivi wallet

Production attributes can be added in the Yivi app using the button "Retrieve my personal data".

Demo attributes can be added via the attribute index webpage [attribute index webpage](https://privacybydesign.foundation/attribute-index/en/).

1. Click on a demo issuer.
2. Click on a credential.
3. Fill in the form at the bottom of the page.
4. Click issue and scan the QR code with your Yivi app.

##### Starting the Yivi server on your machine

Make sure that you have git, go and npm installed.
Make sure that the goPath is exported:

```
export GOPATH="$HOME/go"
PATH="$GOPATH/bin:$PATH"
```

To install the Yivi (IRMA) server run the following commands:

```
git clone https://github.com/privacybydesign/irmago
cd irmago
go install ./irma
```

To start the Yivi (IRMA) server, run:

```
irma server
```

(source: https://irma.app/docs/getting-started/)

#### Running the requestor front-end PoC

First, you have to install the required packages for this PoC. To do this, run the following command in the project folder:

```
npm install
```

Also, make sure that the session URL points to your Yivi server. You can change this on line 61:

```
 url: 'http://192.168.68.110:8088',
```

##### Web

To run this PoC as a web application use the following command:

```
ionic serve
```

##### Android

To run this PoC as an Android webview app use the following command:

```
ionic capacitor add android
ionic cap copy && ionic cap sync && npx cap run android
```

##### iOS

> NOTE the application is not tested on the iOS platform yet

To run this PoC as an iOS webview app use the following command:

```
ionic capacitor add ios
ionic cap copy && ionic cap sync && npx cap run ios
```

#### Request specific attributes

In this PoC we request two attributes from the `irma-demo.discipl.demoBR` credential: `registeredIncome` and `taxYear`. If you want to request different attributes, open `Home.tsx` in your IDE / code editor and update the identifiers in the `identifiersRequestedAttributes` variable on line 41.

```
const identifiersRequestedAttributes = [
    "irma-demo.discipl.demoBRI.registeredIncome",
    "irma-demo.discipl.demoBRI.taxYear"
];
```

#### Specific settings for this PoC

In the `AndroidManifest.xml` the `usesCleartextTraffic` application-flag is set to true, to support communication with the Yivi app over HTTP (instead of HTTPS).

```
android:usesCleartextTraffic="true"
```
